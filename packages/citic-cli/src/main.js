// 解析模块
const { program } = require('commander');
const { version } = require('./constants');
const path = require('path');

const mapActions = {
  create: {
    alias: 'c',
    description: 'create a project',
    examples: [
      'citic-cli create <project-name>',
    ],
  },
  '*': {
    alias: '',
    description: 'command not found',
    examples: [],
  },
};

Object.entries(mapActions).forEach(([action, conf]) => {
  program
    .command(action) // 配置命令的名字
    .alias(conf.alias) // 命令别名
    .description(conf.description) // 对应描述
    .action(() => {
      if (action === '*') {
        console.log(conf.description);
      } else {
        /* eslint-ignore */
        require(path.resolve(__dirname, action))(...process.argv.slice(3));
      }
    });
});

program.on('--help', () => {
  console.log('\nExamples:');
  Object.entries(mapActions).forEach(([action, conf]) => {
    if (action !== '*') {
      conf.examples.forEach((example) => {
        console.log(` ${example}`);
      });
    }
  });
});

program.version(version).parse(process.argv);
