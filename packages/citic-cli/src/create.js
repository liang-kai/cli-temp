// 创建
const axios = require('axios')
const ora = require('ora')
const Inquirer = require('inquirer')
const { promisify } = require('util')
let downloadGitRepo = require('download-git-repo')
const { downloadDir } = require('./constants')
const path = require('path')
const fs = require('fs')
const MetalSmith = require('metalsmith')
const { rejects } = require('assert')
const { resolve } = require('path')
let { render } = require('consolidate').ejs
let ncp = promisify(require('ncp').ncp)

downloadGitRepo = promisify(downloadGitRepo)
render = promisify(render)


const fetchRepoList = async () => {
  try{
    const res = await axios.get('https://gitee.com/api/v5/repos/richole/cli/git/trees/master?recursive=1')
    const repos = []
    res.data.tree.forEach(rep => {
      let name;
      if((name = /^packages\/templates\/([^\/]*)/.exec(rep.path)) && rep.type === 'tree' ){
        // console.log(name[1], rep.path)
        repos.push(name[1])
      }
    })
    // console.log('get repos:', repos)
    return repos
  } catch(e) {
    console.error('获取仓库列表失败')
  }
}
const downloadRepo = async (repo) => {
  const api = `liang-kai/vue-template#main`
  // const api = `https://gitee.com/richole/cli/tree/master/packages/templates/${repo}`
  const dest = `${downloadDir}/${repo}`
  // try{
    await downloadGitRepo(api, dest)
    return dest
  // }catch(e) {
  //   throw new Error('下载仓库失败')
  // }
}

const waitFnLoading = (fn, message) => async (...args) => {
  const spinner = ora(message)
  spinner.start()
  try {
    const res = await fn(...args)
    spinner.succeed()
    return res
  }catch(e) {
    spinner.stop()
    console.error(e)
  }finally{}
}

module.exports = async (projectName) => {
  const repos = await waitFnLoading(fetchRepoList, '获取模板...')()
  const {repo} = await Inquirer.prompt({
    name: 'repo',
    type: 'list',
    message: '请选择一个模板来创建项目',
    choices: repos
  })
  const result = await waitFnLoading(downloadRepo, '下载模板中...')(repo)
  // console.log(repo)

  //todo: check if exsit
  if (!fs.existsSync(path.join(result, 'ask.js'))) {
    await ncp(result, path.resolve(projectName))
  } else {
    await new Promise((resolve, reject) => {
      MetalSmith(__dirname)
      .source(result)
      .destination(path.resolve(projectName))
      .use(async (files, metal, done) => {
        const args = require(path.join(result, 'ask.js'))
        const userSelect = await Inquirer.prompt(args)
        // console.log(userSelect)
        metal = metal.metadata()
        Object.assign(metal, userSelect, {projectName})
        delete files['ask.js']
        done()
      })
      .use((files, metal, done) => {
        // console.log(metal.metadata(), files)
        const userSelect = metal.metadata()

        Reflect.ownKeys(files).forEach( async (file) => {
          if (file.includes('.js') || file.includes('.json')) {
            let content = files[file].contents.toString()
            if (content.includes('<%')) {
              content = await render(content, userSelect)
              files[file].contents = Buffer.from(content)
            }
          }
        })
        done()
      })
      .build( err => {
        if (err) {
          reject()
        } else {
          resolve()
        }
      })
    })
  }
}
