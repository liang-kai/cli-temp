const { version } = require('../package.json');

const downloadDir = process.env[process.platform === 'darwin' ? 'HOME' : 'USERPROFILE'] + '/.templates'
console.log(downloadDir)
module.exports = {
  version,
  downloadDir
};
